-- 3
CREATE VIEW olymp_commission AS
	SELECT
		last_name,
		job_id,
		hire_date        AS start_date,
		hire_date + 8500 AS end_date
	FROM employees;

SELECT
	(SELECT listagg(last_name, ',')
	        WITHIN GROUP (
		        ORDER BY last_name) AS aggregated
	 FROM olymp_commission
	 WHERE job_id = 'AD_PRES'
	       AND (end_date IS NULL OR sysdate BETWEEN start_date AND end_date)) AS "Председатели",
	(SELECT listagg(last_name, ',')
	        WITHIN GROUP (
		        ORDER BY last_name) AS aggregated
	 FROM olymp_commission
	 WHERE job_id = 'IT_PROG'
	       AND (end_date IS NULL OR sysdate BETWEEN start_date AND end_date)) AS "Члены комиссии",
	(SELECT listagg(last_name, ',')
	        WITHIN GROUP (
		        ORDER BY last_name) AS aggregated
	 FROM olymp_commission
	 WHERE job_id = 'AC_ACCOUNT'
	       AND (end_date IS NULL OR sysdate BETWEEN start_date AND end_date)) AS "Секретари комисии"
FROM dual;


-- 5
SELECT
	emp.employee_id       AS emp_id,
	emp.last_name         AS emp_last_name,
	empcntrs.country_name AS emp_country,
	mgr.employee_id       AS mgr_id,
	mgr.last_name         AS mgr_last_name,
	mgrcntrs.country_name AS mgr_country
FROM employees emp
	INNER JOIN departments empdep ON emp.department_id = empdep.department_id
	INNER JOIN locations emploc ON empdep.location_id = emploc.location_id
	INNER JOIN countries empcntrs ON emploc.country_id = empcntrs.country_id
	INNER JOIN employees mgr ON emp.manager_id = mgr.employee_id
	INNER JOIN departments mgrdep ON mgr.department_id = mgrdep.department_id
	INNER JOIN locations mgrloc ON mgrdep.location_id = mgrloc.location_id
	INNER JOIN countries mgrcntrs ON mgrloc.country_id = mgrcntrs.country_id
WHERE emploc.country_id != mgrloc.country_id
ORDER BY empcntrs.country_name, emp.last_name


-- 7
WITH initial_num(num) AS
(
		SELECT (length(&str) - nvl(length(replace(&str, '1')), 0)) * 1 +
		       (length(&str) - nvl(length(replace(&str, '2')), 0)) * 2 +
		       (length(&str) - nvl(length(replace(&str, '3')), 0)) * 3 +
		       (length(&str) - nvl(length(replace(&str, '4')), 0)) * 4 +
		       (length(&str) - nvl(length(replace(&str, '5')), 0)) * 5 +
		       (length(&str) - nvl(length(replace(&str, '6')), 0)) * 6 +
		       (length(&str) - nvl(length(replace(&str, '7')), 0)) * 7 +
		       (length(&str) - nvl(length(replace(&str, '8')), 0)) * 8 +
		       (length(&str) - nvl(length(replace(&str, '9')), 0)) * 9 AS num
		FROM dual
)
	, rec(num) AS
(
	SELECT num
	FROM initial_num

	UNION ALL

	SELECT (length(rec.num) - nvl(length(replace(rec.num, '1')), 0)) * 1 +
	       (length(rec.num) - nvl(length(replace(rec.num, '2')), 0)) * 2 +
	       (length(rec.num) - nvl(length(replace(rec.num, '3')), 0)) * 3 +
	       (length(rec.num) - nvl(length(replace(rec.num, '4')), 0)) * 4 +
	       (length(rec.num) - nvl(length(replace(rec.num, '5')), 0)) * 5 +
	       (length(rec.num) - nvl(length(replace(rec.num, '6')), 0)) * 6 +
	       (length(rec.num) - nvl(length(replace(rec.num, '7')), 0)) * 7 +
	       (length(rec.num) - nvl(length(replace(rec.num, '8')), 0)) * 8 +
	       (length(rec.num) - nvl(length(replace(rec.num, '9')), 0)) * 9 AS num
	FROM rec
	WHERE rec.num > 9
)
SELECT min(num) AS result
FROM rec;


-- 8
WITH hierarchy(lvl, код_специальности, название_специальности, номер_группы, номер_студента, фамилия) AS
(
	SELECT DISTINCT
		1    AS lvl,
		код_специальности,
		название_специальности,
		NULL AS номер_группы,
		NULL AS номер_студента,
		NULL AS фамилия
	FROM
		студенты
		INNER JOIN группы USING (номер_группы)
		INNER JOIN специальности USING (код_специальности)


	UNION
	SELECT DISTINCT
		2    AS lvl,
		код_специальности,
		NULL AS название_специальности,
		номер_группы,
		NULL AS номер_студента,
		NULL AS фамилия
	FROM студенты INNER JOIN группы USING (номер_группы)

	UNION

	SELECT
		3    AS lvl,
		NULL AS код_специальности,
		NULL AS название_специальности,
		номер_группы,
		номер_студента,
		фамилия

	FROM студенты
)
SELECT
	lvl                           AS "Уровень",
	lpad(' ', lvl * 3 - 3) || CASE lvl
	                          WHEN 1
		                          THEN название_специальности
	                          WHEN 2
		                          THEN to_char(номер_группы)
	                          WHEN 3
		                          THEN фамилия
	                          END AS "Иерархия"
FROM hierarchy
START WITH lvl = 1
CONNECT BY PRIOR lvl + 1 = lvl AND
           (lvl = 2 AND PRIOR код_специальности = код_специальности
            OR lvl = 3 AND PRIOR номер_группы = номер_группы)
ORDER SIBLINGS BY название_специальности, номер_группы, фамилия


-- 9
WITH depts_min_salaries AS
(
		SELECT
			department_id,
			min(salary) AS min_salary
		FROM employees
		GROUP BY department_id
		HAVING department_id IS NOT NULL
)
SELECT
	employee_id,
	last_name,
	salary,
	department_id
FROM employees
	INNER JOIN depts_min_salaries USING (department_id)
WHERE salary = min_salary
ORDER BY department_id, salary, last_name


-- 10
WITH workers AS
(
		SELECT
			e1.employee_id   AS employee_id,
			e1.department_id AS department_id
		FROM employees e1
			LEFT OUTER JOIN employees e2 ON (e1.employee_id = e2.manager_id)
		WHERE e2.employee_id IS NULL
)
SELECT
	to_char(department_id),
	workers_count
FROM (SELECT
	      department_id      AS department_id,
	      count(employee_id) AS workers_count
      FROM workers
      GROUP BY department_id
      ORDER BY nvl(department_id, -1))

UNION ALL

SELECT
	'ВСЕГО'            AS department_id,
	count(employee_id) AS workers_count
FROM workers


-- 11
WITH missing_first_job_emps AS
(
	SELECT employee_id
	FROM job_history

	MINUS

	SELECT employee_id
	FROM job_history
		INNER JOIN employees USING (employee_id)
	WHERE start_date = hire_date
)
SELECT
	employee_id,
	last_name,
	hire_date,
	job_id,
	department_id
FROM missing_first_job_emps
	INNER JOIN employees USING (employee_id)
ORDER BY hire_date, last_name;


-- 12
SELECT
	номер_студента,
	listagg(дисциплины.название, ',')
	WITHIN GROUP (
		ORDER BY дисциплины.название)
FROM студенты
	INNER JOIN группы USING (номер_группы)
	INNER JOIN специальности USING (код_специальности)
	INNER JOIN учебные_планы USING (код_специальности)
	INNER JOIN дисциплины USING (номер_дисциплины)
GROUP BY номер_студента;


-- 13
WITH tab(id, des, t) AS (
	SELECT 'A', 'a1', 12 FROM dual UNION
	SELECT 'A','a2', 3 FROM dual UNION
	SELECT 'A', 'a3', 1 FROM dual UNION
	SELECT 'B', 'a1', 10 FROM dual UNION
	SELECT 'B', 'a2', 23 FROM dual UNION
	SELECT 'C', 'a3', 45 FROM dual
)
SELECT DISTINCT
	id,
	(SELECT nvl(sum(t), 0)
	 FROM tab inner
	 WHERE inner.id = outer.id AND inner.des = 'a1') AS "a1",
	(SELECT nvl(sum(t), 0)
	 FROM tab inner
	 WHERE inner.id = outer.id AND inner.des = 'a2') AS "a2",
	(SELECT nvl(sum(t), 0)
	 FROM tab inner
	 WHERE inner.id = outer.id AND inner.des = 'a3') AS "a3"
FROM tab outer
ORDER BY id DESC;


-- 14
WITH lists(list_num, list) AS
(
	SELECT 1, '2162;2199' FROM dual UNION
	SELECT 2, '2152;' FROM dual UNION
	SELECT 3, '39352;' FROM dual UNION
	SELECT 4, '2162' FROM dual UNION
	SELECT 5, '17173' FROM dual UNION
	SELECT 6, '2199' FROM dual UNION
	SELECT 7, '2297' FROM dual UNION
	SELECT 8, '2199;2297' FROM dual
)
	, decomposed_lists (list_num, list, element_num, val) AS
( SELECT
	  list_num,
	  list,
	  regexp_count(list, ';') + 1,
	  regexp_substr(list, ';?([^;]*)$', 1, 1, 'i', 1)
  FROM lists
  UNION ALL
  SELECT
	  list_num,
	  list,
	  element_num - 1,
	  regexp_substr(list, '([^;]*);', 1, element_num - 1, 'i', 1)
  FROM decomposed_lists
  WHERE element_num > 1
),
		graph(parent, child) AS
	(
			SELECT DISTINCT
				l1.list_num AS v1,
				l2.list_num AS v2
			FROM decomposed_lists l1
				INNER JOIN decomposed_lists l2 USING (val)
	),
		graph_with_search_roots (parent, child) AS
	(
		SELECT
			parent,
			child
		FROM graph

		UNION ALL

		SELECT
			NULL,
			parent
		FROM graph
	),
		graph_paths(root, vertex) AS
	(
			SELECT DISTINCT
				CONNECT_BY_ROOT (child) AS root,
				child                   AS path
			FROM graph_with_search_roots
			START WITH parent IS NULL
			CONNECT BY NOCYCLE parent = PRIOR child
	),
		graph_paths_aggregated(root, graph_path) AS
	(
			SELECT DISTINCT
				root,
				LISTAGG(vertex, ' + ')
				WITHIN GROUP (ORDER BY vertex)
				OVER (
					PARTITION BY root ) AS "Связанные множества"
			FROM graph_paths
	),
		connected_components_paths(root, graph_path) AS
	(
			SELECT
				min(root),
				graph_path
			FROM graph_paths_aggregated
			GROUP BY graph_path
	),
		connected_components(root, vertex) AS
	(
			SELECT
				root,
				vertex
			FROM connected_components_paths
				INNER JOIN graph_paths USING (root)
	),
		concatenated_lists AS
	(
			SELECT
				root,
				val
			FROM connected_components
				INNER JOIN decomposed_lists ON (vertex = list_num)
			GROUP BY root, val
	)
SELECT
	listagg(val, ';')
	WITHIN GROUP (ORDER BY val) AS "Объединенный список",
	graph_path                  AS "Объединенные списки"
FROM concatenated_lists
	INNER JOIN graph_paths_aggregated USING (root)
GROUP BY root, graph_path;


-- 15
WITH scale(id) AS
(
	SELECT 10000 FROM dual UNION ALL
	SELECT 30000 FROM dual UNION ALL
	SELECT 45000 FROM dual UNION ALL
	SELECT 55000 FROM dual UNION ALL
	SELECT 70000 FROM dual
)
	, value(val) AS
(
		SELECT 46001
		FROM dual
)
	, bounds(val, left_bound, right_bound) AS
(
		SELECT
			(SELECT val
			 FROM value)   AS val,
			(SELECT id
			 FROM (SELECT
				       id,
				       row_number()
				       OVER (ORDER BY val - id ASC ) AS rg
			       FROM value CROSS JOIN scale
			       WHERE id <= val) ranked
			 WHERE rg = 1) AS left_bound,
			(SELECT id
			 FROM (SELECT
				       id,
				       row_number()
				       OVER (ORDER BY id - val ASC ) AS rg
			       FROM value CROSS JOIN scale
			       WHERE val <= id) ranked
			 WHERE rg = 1) AS right_bound
		FROM dual
)
SELECT
	left_bound,
	right_bound,
	CASE WHEN left_bound IS NULL
		THEN right_bound
	WHEN right_bound IS NULL
		THEN left_bound
	WHEN val - left_bound < right_bound - val
		THEN left_bound
	ELSE right_bound
	END AS closest_bound
FROM bounds


-- 16
WITH lcks(lcker, lcked) AS
(
	SELECT 'A', 'B' FROM dual UNION
	SELECT 'A', 'C' FROM dual UNION
	SELECT 'B', 'D' FROM dual UNION
	SELECT 'D', 'E' FROM dual UNION
	SELECT 'C', 'F' FROM dual UNION
	SELECT 'C', 'J' FROM dual UNION
	SELECT 'C', 'H' FROM dual
)
	, lcks_extended(lcker, lcked) AS
(
	SELECT
		lcker,
		lcked
	FROM lcks

	UNION

	SELECT
		NULL ,
		l1.lcker
	FROM lcks l1
		LEFT OUTER JOIN lcks l2 ON (l1.lcker = l2.lcked)
	WHERE l2.lcked IS NULL
)
SELECT lpad(' ', level - 1) || lcked
FROM lcks_extended
START WITH lcker IS NULL
CONNECT BY PRIOR lcked = lcker;


-- 17
SELECT nvl((SELECT salary
            FROM employees
            WHERE employee_id = &tgt_emp_id), 0) AS salary
FROM dual;


-- 18
WITH d_v(dat, val) AS
(
	SELECT TO_DATE('01-08-08','DD-MM-YY'), 232 FROM dual UNION
	SELECT TO_DATE('02-08-08','DD-MM-YY'), NULL FROM dual UNION
	SELECT TO_DATE('10-08-08','DD-MM-YY'), 182 FROM dual UNION
	SELECT TO_DATE('11-08-08','DD-MM-YY'), NULL FROM dual UNION
	SELECT TO_DATE('21-08-08','DD-MM-YY'), 240 FROM dual UNION
	SELECT TO_DATE('22-08-08','DD-MM-YY'), NULL FROM dual
)
	, dates(dat, corresponding_dat) AS
(
		SELECT
			d1.dat AS dat,
			max(d2.dat)
		FROM d_v d1
			LEFT OUTER JOIN d_v d2 ON d2.dat <= d1.dat AND d2.val IS NOT NULL
		GROUP BY d1.dat, d1.val
)
SELECT dates.dat, val
FROM dates
	INNER JOIN d_v ON (dates.corresponding_dat = d_v.dat);


-- 19
CREATE OR REPLACE VIEW year_2012 AS
	SELECT TO_DATE('01.01.2013', 'DD.MM.YYYY') AS year_d1
	FROM dual;

WITH raw_calendar(month, week_num, nmon, ntue, nwed, nthu, nfri, nsat, nsun) AS
(
		SELECT
			month                                              AS month,
			week_num                                           AS week_num,
			first_monday_of_the_month + (week_num - 2) * 7     AS nmon,
			first_monday_of_the_month + (week_num - 2) * 7 + 1 AS ntue,
			first_monday_of_the_month + (week_num - 2) * 7 + 2 AS nwed,
			first_monday_of_the_month + (week_num - 2) * 7 + 3 AS nthu,
			first_monday_of_the_month + (week_num - 2) * 7 + 4 AS nfri,
			first_monday_of_the_month + (week_num - 2) * 7 + 5 AS nsat,
			first_monday_of_the_month + (week_num - 2) * 7 + 6 AS nsun
		FROM (SELECT
			      ceil(level / 6)                                                         AS month,
			      level - ceil(level / 6) * 6 + 6                                         AS week_num,
			      add_months(year_d1, ceil(level / 6) - 1)
			      + MOD(7 - (TRUNC(add_months(year_d1, ceil(level / 6) - 1))
			                 - TRUNC(add_months(year_d1, ceil(level / 6) - 1), 'IW')), 7) AS first_monday_of_the_month
		      FROM year_2012
		      CONNECT BY level <= 12 * 6)
)
	, checked_calendar(month, weeknum, mon, tue, wed, thu, fri, sat, sun) AS
(
	SELECT
		month,
		week_num,
		CASE WHEN month = extract(MONTH FROM nmon)
			THEN nmon
		ELSE
			NULL
		END,
		CASE WHEN month = extract(MONTH FROM ntue)
			THEN ntue
		ELSE
			NULL
		END,
		CASE WHEN month = extract(MONTH FROM nwed)
			THEN nwed
		ELSE
			NULL
		END,
		CASE WHEN month = extract(MONTH FROM nthu)
			THEN nthu
		ELSE
			NULL
		END,
		CASE WHEN month = extract(MONTH FROM nfri)
			THEN nfri
		ELSE
			NULL
		END,
		CASE WHEN month = extract(MONTH FROM nsat)
			THEN nsat
		ELSE
			NULL
		END,
		CASE WHEN month = extract(MONTH FROM nsun)
			THEN nsun
		ELSE
			NULL
		END
	FROM raw_calendar

	UNION ALL

	SELECT
		level,
		-1,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL
	FROM dual
	CONNECT BY level <= 12

	ORDER BY month, week_num
)
	, months_strings(month, str) AS
(
		SELECT
			level,
			trim(to_char(add_months(to_date('01.01.2000', 'DD.MM.YYYY'), level - 1), 'Month', 'NLS_DATE_LANGUAGE=RUSSIAN'))
			|| ' '
			|| EXTRACT(YEAR FROM (SELECT year_d1
			                      FROM year_2012))
		FROM dual
		CONNECT BY level <= 12
)
SELECT ' пн вт ср чт пт сб вс' AS calendar
FROM dual

UNION ALL

SELECT CASE WHEN weeknum = -1
	THEN
		(
			SELECT str
			FROM months_strings
			WHERE months_strings.month = checked_calendar.month
		)
       ELSE    nvl(to_char(mon, ' DD', 'NLS_DATE_LANGUAGE=RUSSIAN'), ' __')
            || nvl(to_char(tue, ' DD', 'NLS_DATE_LANGUAGE=RUSSIAN'), ' __')
            || nvl(to_char(wed, ' DD', 'NLS_DATE_LANGUAGE=RUSSIAN'), ' __')
            || nvl(to_char(thu, ' DD', 'NLS_DATE_LANGUAGE=RUSSIAN'), ' __')
            || nvl(to_char(fri, ' DD', 'NLS_DATE_LANGUAGE=RUSSIAN'), ' __')
            || nvl(to_char(sat, ' DD', 'NLS_DATE_LANGUAGE=RUSSIAN'), ' __')
            || nvl(to_char(sun, ' DD', 'NLS_DATE_LANGUAGE=RUSSIAN'), ' __')
       END AS calendar
FROM checked_calendar
WHERE
	mon IS NOT NULL OR tue IS NOT NULL OR wed IS NOT NULL OR thu IS NOT NULL OR fri IS NOT NULL OR sat IS NOT NULL OR sun IS NOT NULL
	OR weeknum = -1;


-- 20
WITH lgd(lang_code, next_lang) AS
(
	SELECT 'RUS', 'ENG' FROM dual UNION
	SELECT 'ENG', 'RUS' FROM dual UNION
	SELECT  'UA', 'RUS' FROM dual UNION
	SELECT  'KZ', 'RUS' FROM dual UNION
	SELECT  'BY', 'UA'  FROM dual UNION
	SELECT 'EST', 'ENG' FROM dual
),
		lgd_extended(lang_code, next_lang) AS
	(
		SELECT lang_code, next_lang FROM lgd UNION ALL
		SELECT NULL, 'BY'           FROM dual
	)
SELECT next_lang
FROM lgd_extended
START WITH lang_code IS NULL
CONNECT BY NOCYCLE PRIOR next_lang = lang_code;


