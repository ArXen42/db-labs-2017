WITH lcks(lcker, lcked) AS
(
	SELECT 'A', 'B' FROM dual UNION
	SELECT 'A', 'C' FROM dual UNION
	SELECT 'B', 'D' FROM dual UNION
	SELECT 'D', 'E' FROM dual UNION
	SELECT 'C', 'F' FROM dual UNION
	SELECT 'C', 'J' FROM dual UNION
	SELECT 'C', 'H' FROM dual
)
	, lcks_extended(lcker, lcked) AS
(
	SELECT
		lcker,
		lcked
	FROM lcks

	UNION

	SELECT
		NULL ,
		l1.lcker
	FROM lcks l1
		LEFT OUTER JOIN lcks l2 ON (l1.lcker = l2.lcked)
	WHERE l2.lcked IS NULL
)
SELECT lpad(' ', level - 1) || lcked
FROM lcks_extended
START WITH lcker IS NULL
CONNECT BY PRIOR lcked = lcker;
