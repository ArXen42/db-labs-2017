CREATE VIEW olymp_commission AS
	SELECT
		last_name,
		job_id,
		hire_date        AS start_date,
		hire_date + 8500 AS end_date
	FROM employees;

SELECT
	(SELECT listagg(last_name, ',')
	        WITHIN GROUP (
		        ORDER BY last_name) AS aggregated
	 FROM olymp_commission
	 WHERE job_id = 'AD_PRES'
	       AND (end_date IS NULL OR sysdate BETWEEN start_date AND end_date)) AS "Председатели",
	(SELECT listagg(last_name, ',')
	        WITHIN GROUP (
		        ORDER BY last_name) AS aggregated
	 FROM olymp_commission
	 WHERE job_id = 'IT_PROG'
	       AND (end_date IS NULL OR sysdate BETWEEN start_date AND end_date)) AS "Члены комиссии",
	(SELECT listagg(last_name, ',')
	        WITHIN GROUP (
		        ORDER BY last_name) AS aggregated
	 FROM olymp_commission
	 WHERE job_id = 'AC_ACCOUNT'
	       AND (end_date IS NULL OR sysdate BETWEEN start_date AND end_date)) AS "Секретари комисии"
FROM dual;
