WITH tab(id, des, t) AS (
	SELECT 'A', 'a1', 12 FROM dual UNION
	SELECT 'A','a2', 3 FROM dual UNION
	SELECT 'A', 'a3', 1 FROM dual UNION
	SELECT 'B', 'a1', 10 FROM dual UNION
	SELECT 'B', 'a2', 23 FROM dual UNION
	SELECT 'C', 'a3', 45 FROM dual
)
SELECT DISTINCT
	id,
	(SELECT nvl(sum(t), 0)
	 FROM tab inner
	 WHERE inner.id = outer.id AND inner.des = 'a1') AS "a1",
	(SELECT nvl(sum(t), 0)
	 FROM tab inner
	 WHERE inner.id = outer.id AND inner.des = 'a2') AS "a2",
	(SELECT nvl(sum(t), 0)
	 FROM tab inner
	 WHERE inner.id = outer.id AND inner.des = 'a3') AS "a3"
FROM tab outer
ORDER BY id DESC;
