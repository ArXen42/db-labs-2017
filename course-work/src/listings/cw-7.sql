WITH initial_num(num) AS
(
		SELECT (length(&str) - nvl(length(replace(&str, '1')), 0)) * 1 +
		       (length(&str) - nvl(length(replace(&str, '2')), 0)) * 2 +
		       (length(&str) - nvl(length(replace(&str, '3')), 0)) * 3 +
		       (length(&str) - nvl(length(replace(&str, '4')), 0)) * 4 +
		       (length(&str) - nvl(length(replace(&str, '5')), 0)) * 5 +
		       (length(&str) - nvl(length(replace(&str, '6')), 0)) * 6 +
		       (length(&str) - nvl(length(replace(&str, '7')), 0)) * 7 +
		       (length(&str) - nvl(length(replace(&str, '8')), 0)) * 8 +
		       (length(&str) - nvl(length(replace(&str, '9')), 0)) * 9 AS num
		FROM dual
)
	, rec(num) AS
(
	SELECT num
	FROM initial_num

	UNION ALL

	SELECT (length(rec.num) - nvl(length(replace(rec.num, '1')), 0)) * 1 +
	       (length(rec.num) - nvl(length(replace(rec.num, '2')), 0)) * 2 +
	       (length(rec.num) - nvl(length(replace(rec.num, '3')), 0)) * 3 +
	       (length(rec.num) - nvl(length(replace(rec.num, '4')), 0)) * 4 +
	       (length(rec.num) - nvl(length(replace(rec.num, '5')), 0)) * 5 +
	       (length(rec.num) - nvl(length(replace(rec.num, '6')), 0)) * 6 +
	       (length(rec.num) - nvl(length(replace(rec.num, '7')), 0)) * 7 +
	       (length(rec.num) - nvl(length(replace(rec.num, '8')), 0)) * 8 +
	       (length(rec.num) - nvl(length(replace(rec.num, '9')), 0)) * 9 AS num
	FROM rec
	WHERE rec.num > 9
)
SELECT min(num) AS result
FROM rec;
