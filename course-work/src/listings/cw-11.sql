WITH missing_first_job_emps AS
(
	SELECT employee_id
	FROM job_history

	MINUS

	SELECT employee_id
	FROM job_history
		INNER JOIN employees USING (employee_id)
	WHERE start_date = hire_date
)
SELECT
	employee_id,
	last_name,
	hire_date,
	job_id,
	department_id
FROM missing_first_job_emps
	INNER JOIN employees USING (employee_id)
ORDER BY hire_date, last_name;
