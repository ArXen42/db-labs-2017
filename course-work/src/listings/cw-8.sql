WITH hierarchy(lvl, код_специальности, название_специальности, номер_группы, номер_студента, фамилия) AS
(
	SELECT DISTINCT
		1    AS lvl,
		код_специальности,
		название_специальности,
		NULL AS номер_группы,
		NULL AS номер_студента,
		NULL AS фамилия
	FROM
		студенты
		INNER JOIN группы USING (номер_группы)
		INNER JOIN специальности USING (код_специальности)


	UNION
	SELECT DISTINCT
		2    AS lvl,
		код_специальности,
		NULL AS название_специальности,
		номер_группы,
		NULL AS номер_студента,
		NULL AS фамилия
	FROM студенты INNER JOIN группы USING (номер_группы)

	UNION

	SELECT
		3    AS lvl,
		NULL AS код_специальности,
		NULL AS название_специальности,
		номер_группы,
		номер_студента,
		фамилия

	FROM студенты
)
SELECT
	lvl                           AS "Уровень",
	lpad(' ', lvl * 3 - 3) || CASE lvl
	                          WHEN 1
		                          THEN название_специальности
	                          WHEN 2
		                          THEN to_char(номер_группы)
	                          WHEN 3
		                          THEN фамилия
	                          END AS "Иерархия"
FROM hierarchy
START WITH lvl = 1
CONNECT BY PRIOR lvl + 1 = lvl AND
           (lvl = 2 AND PRIOR код_специальности = код_специальности
            OR lvl = 3 AND PRIOR номер_группы = номер_группы)
ORDER SIBLINGS BY название_специальности, номер_группы, фамилия
