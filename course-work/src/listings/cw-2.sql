/*На стройке лежала куча кирпичей красного, желтого и зеленого цвета. Бригадир строителей скомандовал своим рабочим выкладывать из них треугольную стенку по следующему правилу: для нижнего ряда взять из кучи N произвольных кирпичей, затем на соседние кирпичи одного цвета класть кирпич того же цвета, а на разноцветные — кирпич оставшегося цвета.
Необходимо по первому выложенному ряду кирпичей, представленному в виде:*/

WITH R (n, color) as (
	select 1, 'К' from dual union
	select 2, 'З' from dual union
	select 3, 'Ж' from dual union
	select 4, 'Ж' from dual union
	select 5, 'К' from dual union
	select 6, 'К' from dual)
SELECT
	newcolor,
	count(newcolor)
FROM (SELECT nvl(DECODE(color || PRIOR color,
                        'ЖЖ', 'Ж',
                        'ЖЗ', 'К',
                        'ЖК', 'З',
                        'ЗЗ', 'З',
                        'ЗЖ', 'К',
                        'ЗК', 'Ж',
                        'КК', 'К',
                        'КЖ', 'З',
                        'КЗ', 'Ж'), color) AS newcolor
      FROM r
      CONNECT BY PRIOR n + 1 = n)
GROUP BY newcolor;



WITH R (n, color) as (
	select 1, 'К' from dual union
	select 2, 'З' from dual union
	select 3, 'Ж' from dual union
	select 4, 'Ж' from dual union
	select 5, 'К' from dual union
	select 6, 'К' from dual)
SELECT
	lpad(' ', level) || prior n || PRIOR color || '_' || n || color || '_' || nvl(DECODE(color || PRIOR color,
	        'ЖЖ', 'Ж',
	        'ЖЗ', 'К',
	        'ЖК', 'З',
	        'ЗЗ', 'З',
	        'ЗЖ', 'К',
	        'ЗК', 'Ж',
	        'КК', 'К',
	        'КЖ', 'З',
	        'КЗ', 'Ж'), color) as newcolor
FROM r
CONNECT BY PRIOR n + 1 = n

WITH R (n, color) as (
	select 1, 'К' from dual union
	select 2, 'З' from dual union
	select 3, 'Ж' from dual union
	select 4, 'Ж' from dual union
	select 5, 'К' from dual union
	select 6, 'К' from dual),
		recursive (n, newcolor) AS
	(
		SELECT
			n,
			color AS newcolor
		FROM r

		UNION ALL

		SELECT
			n + 1,
			newcolor
		FROM recursive
		WHERE n < 4
	)
SELECT * FROM recursive;


WITH R (n, color) as (
	select 1, 'К' from dual union
	select 2, 'З' from dual union
	select 3, 'Ж' from dual union
	select 4, 'Ж' from dual union
	select 5, 'К' from dual union
	select 6, 'К' from dual)
SELECT replace((listagg(color,'') WITHIN GROUP (ORDER BY n)),'ЖЖ','A')
FROM r;
