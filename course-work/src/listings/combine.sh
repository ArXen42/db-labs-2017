#!/usr/bin/env bash

nums=(3 5 7 8 9 10 11 12 13 14 15 16 17 18 19 20)

for num in ${nums[*]};
do
 echo -- ${num} >> combined.sql;
 cat "cw-$num.sql" >> combined.sql;
 printf "\n\n" >> combined.sql;
done
