WITH d_v(dat, val) AS
(
	SELECT TO_DATE('01-08-08','DD-MM-YY'), 232 FROM dual UNION
	SELECT TO_DATE('02-08-08','DD-MM-YY'), NULL FROM dual UNION
	SELECT TO_DATE('10-08-08','DD-MM-YY'), 182 FROM dual UNION
	SELECT TO_DATE('11-08-08','DD-MM-YY'), NULL FROM dual UNION
	SELECT TO_DATE('21-08-08','DD-MM-YY'), 240 FROM dual UNION
	SELECT TO_DATE('22-08-08','DD-MM-YY'), NULL FROM dual
)
	, dates(dat, corresponding_dat) AS
(
		SELECT
			d1.dat AS dat,
			max(d2.dat)
		FROM d_v d1
			LEFT OUTER JOIN d_v d2 ON d2.dat <= d1.dat AND d2.val IS NOT NULL
		GROUP BY d1.dat, d1.val
)
SELECT dates.dat, val
FROM dates
	INNER JOIN d_v ON (dates.corresponding_dat = d_v.dat);
