WITH lists(list_num, list) AS
(
	SELECT 1, '2162;2199' FROM dual UNION
	SELECT 2, '2152;' FROM dual UNION
	SELECT 3, '39352;' FROM dual UNION
	SELECT 4, '2162' FROM dual UNION
	SELECT 5, '17173' FROM dual UNION
	SELECT 6, '2199' FROM dual UNION
	SELECT 7, '2297' FROM dual UNION
	SELECT 8, '2199;2297' FROM dual
)
	, decomposed_lists (list_num, list, element_num, val) AS
( SELECT
	  list_num,
	  list,
	  regexp_count(list, ';') + 1,
	  regexp_substr(list, ';?([^;]*)$', 1, 1, 'i', 1)
  FROM lists
  UNION ALL
  SELECT
	  list_num,
	  list,
	  element_num - 1,
	  regexp_substr(list, '([^;]*);', 1, element_num - 1, 'i', 1)
  FROM decomposed_lists
  WHERE element_num > 1
),
		graph(parent, child) AS
	(
			SELECT DISTINCT
				l1.list_num AS v1,
				l2.list_num AS v2
			FROM decomposed_lists l1
				INNER JOIN decomposed_lists l2 USING (val)
	),
		graph_with_search_roots (parent, child) AS
	(
		SELECT
			parent,
			child
		FROM graph

		UNION ALL

		SELECT
			NULL,
			parent
		FROM graph
	),
		graph_paths(root, vertex) AS
	(
			SELECT DISTINCT
				CONNECT_BY_ROOT (child) AS root,
				child                   AS path
			FROM graph_with_search_roots
			START WITH parent IS NULL
			CONNECT BY NOCYCLE parent = PRIOR child
	),
		graph_paths_aggregated(root, graph_path) AS
	(
			SELECT DISTINCT
				root,
				LISTAGG(vertex, ' + ')
				WITHIN GROUP (ORDER BY vertex)
				OVER (
					PARTITION BY root ) AS "Связанные множества"
			FROM graph_paths
	),
		connected_components_paths(root, graph_path) AS
	(
			SELECT
				min(root),
				graph_path
			FROM graph_paths_aggregated
			GROUP BY graph_path
	),
		connected_components(root, vertex) AS
	(
			SELECT
				root,
				vertex
			FROM connected_components_paths
				INNER JOIN graph_paths USING (root)
	),
		concatenated_lists AS
	(
			SELECT
				root,
				val
			FROM connected_components
				INNER JOIN decomposed_lists ON (vertex = list_num)
			GROUP BY root, val
	)
SELECT
	listagg(val, ';')
	WITHIN GROUP (ORDER BY val) AS "Объединенный список",
	graph_path                  AS "Объединенные списки"
FROM concatenated_lists
	INNER JOIN graph_paths_aggregated USING (root)
GROUP BY root, graph_path;
