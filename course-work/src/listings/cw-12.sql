SELECT
	номер_студента,
	listagg(дисциплины.название, ',')
	WITHIN GROUP (
		ORDER BY дисциплины.название)
FROM студенты
	INNER JOIN группы USING (номер_группы)
	INNER JOIN специальности USING (код_специальности)
	INNER JOIN учебные_планы USING (код_специальности)
	INNER JOIN дисциплины USING (номер_дисциплины)
GROUP BY номер_студента;
