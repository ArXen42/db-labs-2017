WITH depts_min_salaries AS
(
		SELECT
			department_id,
			min(salary) AS min_salary
		FROM employees
		GROUP BY department_id
		HAVING department_id IS NOT NULL
)
SELECT
	employee_id,
	last_name,
	salary,
	department_id
FROM employees
	INNER JOIN depts_min_salaries USING (department_id)
WHERE salary = min_salary
ORDER BY department_id, salary, last_name
