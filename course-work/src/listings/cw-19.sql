CREATE OR REPLACE VIEW year_2012 AS
	SELECT TO_DATE('01.01.2013', 'DD.MM.YYYY') AS year_d1
	FROM dual;

WITH raw_calendar(month, week_num, nmon, ntue, nwed, nthu, nfri, nsat, nsun) AS
(
		SELECT
			month                                              AS month,
			week_num                                           AS week_num,
			first_monday_of_the_month + (week_num - 2) * 7     AS nmon,
			first_monday_of_the_month + (week_num - 2) * 7 + 1 AS ntue,
			first_monday_of_the_month + (week_num - 2) * 7 + 2 AS nwed,
			first_monday_of_the_month + (week_num - 2) * 7 + 3 AS nthu,
			first_monday_of_the_month + (week_num - 2) * 7 + 4 AS nfri,
			first_monday_of_the_month + (week_num - 2) * 7 + 5 AS nsat,
			first_monday_of_the_month + (week_num - 2) * 7 + 6 AS nsun
		FROM (SELECT
			      ceil(level / 6)                                                         AS month,
			      level - ceil(level / 6) * 6 + 6                                         AS week_num,
			      add_months(year_d1, ceil(level / 6) - 1)
			      + MOD(7 - (TRUNC(add_months(year_d1, ceil(level / 6) - 1))
			                 - TRUNC(add_months(year_d1, ceil(level / 6) - 1), 'IW')), 7) AS first_monday_of_the_month
		      FROM year_2012
		      CONNECT BY level <= 12 * 6)
)
	, checked_calendar(month, weeknum, mon, tue, wed, thu, fri, sat, sun) AS
(
	SELECT
		month,
		week_num,
		CASE WHEN month = extract(MONTH FROM nmon)
			THEN nmon
		ELSE
			NULL
		END,
		CASE WHEN month = extract(MONTH FROM ntue)
			THEN ntue
		ELSE
			NULL
		END,
		CASE WHEN month = extract(MONTH FROM nwed)
			THEN nwed
		ELSE
			NULL
		END,
		CASE WHEN month = extract(MONTH FROM nthu)
			THEN nthu
		ELSE
			NULL
		END,
		CASE WHEN month = extract(MONTH FROM nfri)
			THEN nfri
		ELSE
			NULL
		END,
		CASE WHEN month = extract(MONTH FROM nsat)
			THEN nsat
		ELSE
			NULL
		END,
		CASE WHEN month = extract(MONTH FROM nsun)
			THEN nsun
		ELSE
			NULL
		END
	FROM raw_calendar

	UNION ALL

	SELECT
		level,
		-1,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL
	FROM dual
	CONNECT BY level <= 12

	ORDER BY month, week_num
)
	, months_strings(month, str) AS
(
		SELECT
			level,
			trim(to_char(add_months(to_date('01.01.2000', 'DD.MM.YYYY'), level - 1), 'Month', 'NLS_DATE_LANGUAGE=RUSSIAN'))
			|| ' '
			|| EXTRACT(YEAR FROM (SELECT year_d1
			                      FROM year_2012))
		FROM dual
		CONNECT BY level <= 12
)
SELECT ' пн вт ср чт пт сб вс' AS calendar
FROM dual

UNION ALL

SELECT CASE WHEN weeknum = -1
	THEN
		(
			SELECT str
			FROM months_strings
			WHERE months_strings.month = checked_calendar.month
		)
       ELSE    nvl(to_char(mon, ' DD', 'NLS_DATE_LANGUAGE=RUSSIAN'), ' __')
            || nvl(to_char(tue, ' DD', 'NLS_DATE_LANGUAGE=RUSSIAN'), ' __')
            || nvl(to_char(wed, ' DD', 'NLS_DATE_LANGUAGE=RUSSIAN'), ' __')
            || nvl(to_char(thu, ' DD', 'NLS_DATE_LANGUAGE=RUSSIAN'), ' __')
            || nvl(to_char(fri, ' DD', 'NLS_DATE_LANGUAGE=RUSSIAN'), ' __')
            || nvl(to_char(sat, ' DD', 'NLS_DATE_LANGUAGE=RUSSIAN'), ' __')
            || nvl(to_char(sun, ' DD', 'NLS_DATE_LANGUAGE=RUSSIAN'), ' __')
       END AS calendar
FROM checked_calendar
WHERE
	mon IS NOT NULL OR tue IS NOT NULL OR wed IS NOT NULL OR thu IS NOT NULL OR fri IS NOT NULL OR sat IS NOT NULL OR sun IS NOT NULL
	OR weeknum = -1;
