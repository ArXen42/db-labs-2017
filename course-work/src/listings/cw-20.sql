WITH lgd(lang_code, next_lang) AS
(
	SELECT 'RUS', 'ENG' FROM dual UNION
	SELECT 'ENG', 'RUS' FROM dual UNION
	SELECT  'UA', 'RUS' FROM dual UNION
	SELECT  'KZ', 'RUS' FROM dual UNION
	SELECT  'BY', 'UA'  FROM dual UNION
	SELECT 'EST', 'ENG' FROM dual
),
		lgd_extended(lang_code, next_lang) AS
	(
		SELECT lang_code, next_lang FROM lgd UNION ALL
		SELECT NULL, 'BY'           FROM dual
	)
SELECT next_lang
FROM lgd_extended
START WITH lang_code IS NULL
CONNECT BY NOCYCLE PRIOR next_lang = lang_code;
