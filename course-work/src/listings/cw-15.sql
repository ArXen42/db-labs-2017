WITH scale(id) AS
(
	SELECT 10000 FROM dual UNION ALL
	SELECT 30000 FROM dual UNION ALL
	SELECT 45000 FROM dual UNION ALL
	SELECT 55000 FROM dual UNION ALL
	SELECT 70000 FROM dual
)
	, value(val) AS
(
		SELECT 46001
		FROM dual
)
	, bounds(val, left_bound, right_bound) AS
(
		SELECT
			(SELECT val
			 FROM value)   AS val,
			(SELECT id
			 FROM (SELECT
				       id,
				       row_number()
				       OVER (ORDER BY val - id ASC ) AS rg
			       FROM value CROSS JOIN scale
			       WHERE id <= val) ranked
			 WHERE rg = 1) AS left_bound,
			(SELECT id
			 FROM (SELECT
				       id,
				       row_number()
				       OVER (ORDER BY id - val ASC ) AS rg
			       FROM value CROSS JOIN scale
			       WHERE val <= id) ranked
			 WHERE rg = 1) AS right_bound
		FROM dual
)
SELECT
	left_bound,
	right_bound,
	CASE WHEN left_bound IS NULL
		THEN right_bound
	WHEN right_bound IS NULL
		THEN left_bound
	WHEN val - left_bound < right_bound - val
		THEN left_bound
	ELSE right_bound
	END AS closest_bound
FROM bounds
