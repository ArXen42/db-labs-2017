WITH workers AS
(
		SELECT
			e1.employee_id   AS employee_id,
			e1.department_id AS department_id
		FROM employees e1
			LEFT OUTER JOIN employees e2 ON (e1.employee_id = e2.manager_id)
		WHERE e2.employee_id IS NULL
)
SELECT
	to_char(department_id),
	workers_count
FROM (SELECT
	      department_id      AS department_id,
	      count(employee_id) AS workers_count
      FROM workers
      GROUP BY department_id
      ORDER BY nvl(department_id, -1))

UNION ALL

SELECT
	'ВСЕГО'            AS department_id,
	count(employee_id) AS workers_count
FROM workers
