SELECT
	emp.employee_id       AS emp_id,
	emp.last_name         AS emp_last_name,
	empcntrs.country_name AS emp_country,
	mgr.employee_id       AS mgr_id,
	mgr.last_name         AS mgr_last_name,
	mgrcntrs.country_name AS mgr_country
FROM employees emp
	INNER JOIN departments empdep ON emp.department_id = empdep.department_id
	INNER JOIN locations emploc ON empdep.location_id = emploc.location_id
	INNER JOIN countries empcntrs ON emploc.country_id = empcntrs.country_id
	INNER JOIN employees mgr ON emp.manager_id = mgr.employee_id
	INNER JOIN departments mgrdep ON mgr.department_id = mgrdep.department_id
	INNER JOIN locations mgrloc ON mgrdep.location_id = mgrloc.location_id
	INNER JOIN countries mgrcntrs ON mgrloc.country_id = mgrcntrs.country_id
WHERE emploc.country_id != mgrloc.country_id
ORDER BY empcntrs.country_name, emp.last_name
