-- noinspection SqlCheckUsingColumnsForFile

-- noinspection SqlResolveForFile

CREATE TABLE test
(
	col1 NUMBER(12),
	col2 NUMBER(12),
	col3 NUMBER(12),
	PRIMARY KEY (col1, col2, col3),
	CONSTRAINT test_check1 CHECK (col1 + col2 + col3 > 0),
	CONSTRAINT test_check2 CHECK (col1 + col2 + col3 > 1),
	CONSTRAINT test_check3 CHECK (col1 + col2 + col3 > 2)
);

INSERT INTO test(col1, col2, col3)
VALUES
	(1, 2, 3);

INSERT INTO test(col1, col2, col3)
VALUES
	(2, 3, 4);


SELECT *
FROM test;

--3.6
WITH
	first_checks AS (SELECT table_name,
		                 min(constraint_name) AS constraint_name,
		                 count(constraint_name) AS overall_checks_count
	                 FROM sys.user_constraints
	                 WHERE constraint_type = 'C'
	                 GROUP BY table_name)
SELECT table_name,
	constraint_name AS check_name,
	column_name,
	nullable,
	overall_checks_count
FROM sys.user_tables tbs
	     LEFT OUTER JOIN first_checks fchk USING (table_name)
	     LEFT OUTER JOIN sys.user_cons_columns ucc USING (table_name, constraint_name)
	     INNER JOIN sys.user_tab_cols utc USING (table_name, column_name);
