--5-1
SELECT
	department_name      AS "Name",
	location_id          AS "Location",
	count(department_id) AS "Number of people",
	avg(salary)          AS "Salary"
FROM departments
	INNER JOIN employees USING (department_id)
GROUP BY department_id, department_name, location_id;

--5-2
SELECT
	last_name,
	department_id,
	department_name
FROM employees
	LEFT OUTER JOIN departments USING (department_id);

--5-3
SELECT DISTINCT
	job_id,
	location_id
FROM departments
	INNER JOIN employees USING (department_id)
WHERE department_id = 80;

--5-4
SELECT
	last_name,
	department_name
FROM employees
	LEFT OUTER JOIN departments USING (department_id)
WHERE last_name LIKE '%a%';

--5-5
SELECT
	last_name,
	job_id,
	department_id,
	department_name
FROM employees
	INNER JOIN departments USING (department_id)
	INNER JOIN locations USING (location_id)
WHERE city = 'Toronto';

--5-6
SELECT
	emp1.last_name   AS "Employee",
	emp1.employee_id AS "Emp#",
	emp2.last_name   AS "Manager",
	emp2.employee_id AS "Mgr#"
FROM employees emp1
	LEFT OUTER JOIN employees emp2 ON emp1.manager_id = emp2.employee_id;

--5-7
SELECT
	employees1.last_name   AS "Employee",
	employees1.employee_id AS "Emp#",
	employees2.last_name   AS "Manager",
	employees2.employee_id AS "Mgr#"
FROM employees employees1
	LEFT OUTER JOIN employees employees2 ON employees1.manager_id = employees2.employee_id
ORDER BY employees1.employee_id;

--5-8
SELECT
	employees1.department_id AS "Department ID",
	employees1.last_name     AS "Employee",
	employees2.last_name     AS "Colleague"
FROM employees employees1
	INNER JOIN employees employees2 ON employees1.department_id = employees2.department_id;

--5-9
SELECT
	last_name,
	job_id,
	department_name,
	salary,
	grade_level
FROM employees
	LEFT OUTER JOIN departments USING (department_id)
	LEFT OUTER JOIN job_grades ON salary BETWEEN lowest_sal AND highest_sal;

--5-10
SELECT
	employees.last_name,
	employees.hire_date
FROM employees
	INNER JOIN employees emp2 ON employees.hire_date > emp2.hire_date
WHERE emp2.last_name = 'Davies';

--5-11
SELECT
	emp.last_name AS "Employee",
	emp.hire_date AS "Emp Hired",
	mgr.last_name AS "Manager",
	mgr.hire_date AS "Mgr hired"
FROM employees emp INNER JOIN employees mgr
		ON emp.manager_id = mgr.employee_id
WHERE emp.hire_date < mgr.hire_date;

--5-12
SELECT
	departments.department_id,
	department_name,
	location_id,
	count(employee_id)
FROM departments
	LEFT OUTER JOIN employees ON employees.department_id = departments.department_id
GROUP BY departments.department_id, department_name, location_id;

--5-13
SELECT DISTINCT
	job_id,
	count(employee_id) AS frequency
FROM employees
	INNER JOIN departments USING (department_id)
WHERE department_name IN ('Administration', 'Executive')
GROUP BY job_id
ORDER BY frequency DESC;

--5-14
SELECT
	emp.last_name,
	mgr.last_name,
	mgr.salary,
	grade_level AS gra
FROM employees emp
	INNER JOIN employees mgr ON emp.manager_id = mgr.employee_id
	LEFT OUTER JOIN job_grades ON mgr.salary BETWEEN lowest_sal AND highest_sal
WHERE mgr.salary > 15000;

--5-15
SELECT
	employees.last_name,
	department_name,
	location_id,
	city
FROM employees
	LEFT OUTER JOIN departments USING (department_id)
	LEFT OUTER JOIN locations USING (location_id)
WHERE nvl(employees.commission_pct, 0) > 0;

--6-1
SELECT
	last_name,
	hire_date
FROM employees
WHERE department_id IN (SELECT department_id
                        FROM employees
                        WHERE last_name = 'Zlotkey')
      AND last_name != 'Zlotkey';

--6-2
SELECT
	employee_id,
	last_name
FROM employees
WHERE salary > (SELECT avg(salary)
                FROM employees)
ORDER BY salary ASC;

--6-3
SELECT
	employee_id,
	last_name
FROM employees
WHERE department_id IN (SELECT department_id
                        FROM employees
                        WHERE last_name LIKE '%u%');

--6-4
SELECT
	last_name,
	department_id,
	job_id
FROM employees
WHERE (SELECT location_id
       FROM departments
       WHERE departments.department_id = employees.department_id) = 1700;

--6-5
SELECT
	last_name,
	salary
FROM employees
WHERE manager_id IN (SELECT employee_id
                     FROM employees
                     WHERE last_name = 'King');
-- В базе два Кинга (id 100 и 156), запрос работать не будет.
-- Если необходимо выбрать подчиненных обеих Кингов, достаточно заменить "=" на "IN".

--6-6
SELECT
	department_id,
	last_name,
	job_id
FROM employees
WHERE department_id IN (SELECT department_id
                        FROM departments
                        WHERE department_name = 'Executive');

--6-7
SELECT
	employee_id,
	last_name,
	salary
FROM employees
WHERE department_id IN (SELECT department_id
                        FROM employees
                        WHERE last_name LIKE '%u%')
      AND salary > (SELECT avg(salary)
                    FROM employees);

--6-8
SELECT
	department_id,
	min(salary)
FROM employees
GROUP BY department_id
HAVING avg(salary) = (SELECT max(avg(salary)) AS avgsal
                      FROM employees
                      GROUP BY department_id);

--6-9
SELECT
	department_id,
	department_name,
	location_id
FROM departments
WHERE department_id NOT IN (SELECT DISTINCT department_id
                            FROM employees
                            WHERE job_id = 'SA_REP' AND department_id IS NOT NULL);

--7-1
SELECT department_id
FROM departments
MINUS
SELECT DISTINCT department_id
FROM employees
WHERE job_id = 'ST_CLERK';

--7-2
SELECT
	location_id,
	country_id
FROM
	(SELECT location_id
	 FROM locations
	 MINUS
	 SELECT DISTINCT location_id
	 FROM departments) INNER JOIN locations USING (location_id);

--7-3
SELECT
	job_id,
	department_id
FROM (SELECT DISTINCT
	      job_id,
	      department_id,
	      0 AS priority
      FROM employees
      WHERE department_id = 10

      UNION

      SELECT DISTINCT
	      job_id,
	      department_id,
	      1 AS priority
      FROM employees
      WHERE department_id = 50

      UNION

      SELECT DISTINCT
	      job_id,
	      department_id,
	      2 AS priority
      FROM employees
      WHERE department_id = 20
      ORDER BY priority);

SELECT DISTINCT
	job_id,
	department_id
FROM employees
WHERE department_id = 10

UNION ALL

SELECT DISTINCT
	job_id,
	department_id
FROM employees
WHERE department_id = 50

UNION ALL

SELECT DISTINCT
	job_id,
	department_id
FROM employees
WHERE department_id = 20;

--7-4
SELECT
	employee_id,
	job_id
FROM employees
WHERE job_id IN (SELECT job_id
                 FROM job_history
                 WHERE job_history.employee_id = employees.employee_id);

--7-5
(
	SELECT
		employee_id,
		department_id,
		'' AS department_name
	FROM employees
)
UNION ALL
(
	SELECT
		NULL AS employee_id,
		department_id,
		department_name
	FROM departments
);

--8-1 (script)
CREATE TABLE my_employee
(
	id         NUMBER(4) CONSTRAINT my_employee_id_nn NOT NULL,
	last_name  VARCHAR2(25),
	first_name VARCHAR2(25),
	userid     VARCHAR2(8),
	salary     NUMBER(9, 2)
);

--8-2
DESCRIBE my_employee

--8-3
INSERT INTO my_employee VALUES (1, 'Patel', 'Ralph', 'rpatel', 895);

--8-4
INSERT INTO my_employee (id, last_name, first_name, userid, salary) VALUES (2, 'Dancs', 'Betty', 'bdanc', 860);

--8-6,7
INSERT INTO my_employee (id, last_name, first_name, userid, salary) VALUES (&id,
                                                                            &lname,
                                                                            &fname,
                                                                            lower(substr(&fname, 0, 1) ||
                                                                                  substr(&lname, 0, 7)),
                                                                            &sal);

--8-9
COMMIT;

--8-10
UPDATE my_employee
SET last_name = 'Drexler'
WHERE id = 3;

--8-11
UPDATE my_employee
SET salary = 1000
WHERE salary < 900;

--8-13
DELETE FROM my_employee
WHERE last_name = 'Dancs' AND first_name = 'Betty';

--8-15
COMMIT;

--8-18
SAVEPOINT andrey_inserted;

--8-19
DELETE FROM my_employee;

--8-21
ROLLBACK TO andrey_inserted;

--8-19
DELETE FROM my_employee;

--8-23
COMMIT;
