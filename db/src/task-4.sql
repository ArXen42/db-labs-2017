WITH numbers(num) AS -- Возвращает номера от 1 до 366
     (SELECT rownum AS num
      FROM (SELECT 1 AS tmpnum --Данный подзапрос вернет 2^9 строк
            FROM dual
            GROUP BY CUBE (1, 2, 3, 4, 5, 6, 7, 8, 9))
      WHERE rownum <= 366),
     dates(dt) AS -- Получение списка дат
     (SELECT to_date('01.01.2013', 'DD.MM.YYYY') + num - 1 FROM numbers)
SELECT to_char(dt, 'DD.MM.YYYY') AS "Date", to_char(dt, 'Day') AS "Day"
FROM dates
WHERE extract(YEAR FROM dt) = 2013; --Отсечение 366 дня, если год не високосный


--4.2
CREATE TABLE init
(
  param VARCHAR2(255),
  val   VARCHAR2(255)
)
ORGANIZATION EXTERNAL
  (TYPE ORACLE_LOADER
  DEFAULT DIRECTORY stud
  ACCESS PARAMETERS
  (RECORDS DELIMITED BY NEWLINE
    LOAD WHEN (param != BLANKS AND val != BLANKS)
    BADFILE 'geopela_init_bad.txt'
    LOGFILE 'geopela_init_log.txt'
    FIELDS TERMINATED BY '='
    MISSING FIELD VALUES ARE NULL
      (param
      CHAR (255) LTRIM,
        val
        CHAR (255) LTRIM
      ))
  LOCATION ('init.ora'))
  REJECT LIMIT 0;

DROP TABLE init PURGE;

CREATE TABLE init_dump AS
  SELECT *
  FROM init;

SELECT *
FROM init;

SELECT regexp_count(val, '".*?"') AS ctl_files_cnt
FROM init_dump
WHERE param = 'control_files';

-- Helpers

CREATE TABLE init_plain_text
(
  ln VARCHAR2(1024)
)
ORGANIZATION EXTERNAL
  (TYPE ORACLE_LOADER
  DEFAULT DIRECTORY stud
  ACCESS PARAMETERS
  (RECORDS DELIMITED BY NEWLINE
    NOBADFILE
    NOLOGFILE
    FIELDS TERMINATED BY ','
      (ln
      CHAR (1024)
      ))
  LOCATION ('init.ora'))
PARALLEL 5
  REJECT LIMIT 1024;

CREATE TABLE init_log
(
  ln VARCHAR2(1024)
)
ORGANIZATION EXTERNAL
  (TYPE ORACLE_LOADER
  DEFAULT DIRECTORY stud
  ACCESS PARAMETERS
  (RECORDS DELIMITED BY NEWLINE
    NOBADFILE
    NOLOGFILE
    FIELDS TERMINATED BY ','
      (ln
      CHAR (1024)
      ))
  LOCATION ('geopela_init_log.txt'))
PARALLEL 5
  REJECT LIMIT 1024;

CREATE TABLE init_bad
(
  ln VARCHAR2(1024)
)
ORGANIZATION EXTERNAL
  (TYPE ORACLE_LOADER
  DEFAULT DIRECTORY stud
  ACCESS PARAMETERS
  (RECORDS DELIMITED BY NEWLINE
    NOBADFILE
    NOLOGFILE
    FIELDS TERMINATED BY ','
      (ln
      CHAR (1024)
      ))
  LOCATION ('geopela_init_bad.txt'))
PARALLEL 5
  REJECT LIMIT 0;

DROP TABLE init_log PURGE;

SELECT *
FROM init_plain_text;

SELECT *
FROM init_log;

SELECT *
FROM init_bad;
