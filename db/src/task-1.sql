--1.5
WITH data AS (SELECT to_date('25.06.2015', 'DD.MM.SYYYY') AS initial_dat,
                     to_date('26.08.2039', 'DD.MM.SYYYY') AS current_dat
              FROM dual),
     normalized_data AS (SELECT CASE
                                  WHEN initial_dat < current_dat
                                          THEN initial_dat
                                  ELSE current_dat END AS initial_dat,
                                CASE
                                  WHEN initial_dat < current_dat
                                          THEN current_dat
                                  ELSE initial_dat END AS current_dat
                         FROM data),
     intermediate AS (SELECT initial_dat, current_dat, floor(months_between(current_dat, initial_dat)) AS full_months
                      FROM normalized_data),
     intermediate2 AS (SELECT initial_dat, current_dat, full_months, floor(full_months / 12) AS full_years
                       FROM intermediate),
     intermediate3 AS (SELECT initial_dat,
                              current_dat,
                              full_years,
                              full_months - full_years * 12        AS remaining_months,
                              add_months(initial_dat, full_months) AS initial_plus_full_months
                       FROM intermediate2),
     results AS (SELECT initial_dat,
                        current_dat,
                        full_years,
                        remaining_months,
                        floor(current_dat - initial_plus_full_months) AS remaining_days,
                        MOD(trunc(full_years / 10), 10)               AS decades_count,
                        MOD(full_years, 10)                           AS years_remainder
                 FROM intermediate3),
     formatted AS (SELECT CASE
                            WHEN full_years = 0
                                    THEN ''
                            WHEN years_remainder >= 5 OR decades_count = 1
                                    THEN full_years || ' лет'
                            WHEN years_remainder < 2
                                    THEN full_years || ' год'
                            WHEN years_remainder < 5
                                    THEN full_years || ' года' END AS "Y",
                          CASE
                            WHEN remaining_months = 0
                                    THEN ''
                            ELSE remaining_months || ' мес' END    AS "M",
                          CASE
                            WHEN remaining_days = 0
                                    THEN ''
                            ELSE remaining_days || ' дн' END       AS "D"
                   FROM results)
SELECT *
FROM formatted;

SELECT nullif(nullif(nullif(0, 1), 1), 0)
FROM dual;

select nullif(0, null)
from dual
