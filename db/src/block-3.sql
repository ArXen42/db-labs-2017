--9-1
CREATE TABLE dept
(
	id   NUMBER(7),
	name VARCHAR2(25)
);

--9-2
INSERT INTO dept (id, name)
	SELECT
		department_id,
		department_name
	FROM departments;

--9-3
CREATE TABLE emp
(
	id         NUMBER(7),
	last_name  VARCHAR2(25),
	first_name VARCHAR2(25),
	dept_id    NUMBER(7)
);

--9-4
CREATE TABLE employees2 AS
	SELECT
		employee_id   AS id,
		first_name    AS first_name,
		last_name     AS last_name,
		salary        AS salary,
		department_id AS dept_id
	FROM employees;

--9-5
DROP TABLE emp;

--10-1-1
CREATE VIEW employees_vu AS
	SELECT
		employee_id   AS employee_id,
		last_name     AS employee,
		department_id AS department_id
	FROM employees;

--10-1-2
SELECT *
FROM employees_vu;

--10-1-3
SELECT
	employee,
	department_id
FROM employees_vu;

--10-1-4
CREATE VIEW dept50 AS
	SELECT
		employee_id   AS employee_id,
		last_name     AS employee,
		department_id AS department_id
	FROM employees
	WHERE department_id = 50
	WITH CHECK OPTION CONSTRAINT dept50_ck;

--10-1-5
SELECT *
FROM dept50;

--10-1-6
UPDATE dept50
SET department_id = 80
WHERE employee_id = 143; -- или employee = 'Matos'

--10-1-7
CREATE VIEW salary_vu AS
	SELECT
		last_name       AS "Employee",
		department_name AS "Department",
		salary          AS "Salary",
		grade_level     AS "Grade"
	FROM employees
		LEFT OUTER JOIN departments USING (department_id)
		LEFT OUTER JOIN job_grades ON salary BETWEEN lowest_sal AND highest_sal;

--10-2-1
-- noinspection SpellCheckingInspection
CREATE SEQUENCE dept_id_seq
START WITH 200
INCREMENT BY 10
MAXVALUE 1000
NOCYCLE;

--10-2-2
INSERT INTO dept (id, name) VALUES (
	dept_id_seq.nextval,
	'Education'
);

INSERT INTO dept (id, name) VALUES (
	dept_id_seq.nextval,
	'Administration'
);

--10-2-3
ALTER TABLE employees2
	ADD CONSTRAINT employees2_dept_id_fk
FOREIGN KEY (dept_id) REFERENCES departments (department_id);

CREATE INDEX dept_dept_id_index
	ON employees2 (dept_id);

--10-2-4
CREATE SYNONYM emp
FOR employees;

--11-1
SELECT
	column_name    AS column_name,
	data_type      AS data_type,
	data_length    AS data_length,
	data_precision AS precision,
	data_scale     AS scale,
	nullable       AS nullable
FROM sys.user_tab_columns
WHERE UPPER(table_name) = UPPER(&table_name);

--11-2
SELECT
	column_name,
	constraint_name,
	search_condition,
	status
FROM sys.user_cons_columns
	INNER JOIN sys.user_constraints USING (constraint_name)
WHERE upper(sys.user_cons_columns.table_name) = upper(&tname);

--11-3
COMMENT ON TABLE departments IS '11.3 Commentary';

SELECT comments
FROM sys.user_tab_comments
WHERE UPPER(table_name) = 'DEPARTMENTS';

--11.4
SELECT *
FROM sys.user_synonyms;

--11.5
SELECT
	view_name,
	text
FROM sys.user_views;

--11.6
SELECT
	sequence_name,
	max_value,
	increment_by,
	last_number
FROM sys.user_sequences;