--1-6
CREATE USER geopel_test_user
IDENTIFIED BY Mellon;

GRANT ALL PRIVILEGES
ON geopela.departments
TO geopel_test_user;

GRANT CREATE SYNONYM
TO geopel_test_user;

--1-7
SELECT *
FROM geopela.departments;

--1-8
INSERT INTO departments (department_id, department_name, manager_id, location_id)
VALUES (500, 'Education', NULL, 1700);

INSERT INTO departments (department_id, department_name, manager_id, location_id)
VALUES (510, 'Training', NULL, 1700);

--1-9
CREATE SYNONYM geopel_test_user.test_departments FOR geopela.departments;

--1-10
SELECT *
FROM geopel_test_user.test_departments;

--1-11
SELECT *
FROM sys.user_tables;

--1-12
SELECT table_name
FROM sys.all_tables
MINUS
SELECT table_name
FROM sys.user_tables;

--1-13
REVOKE SELECT
ON geopela.departments
FROM geopel_test_user;

--1-14
DELETE FROM departments
WHERE department_id = 500 OR department_id = 510;
COMMIT;

--2-1
CREATE TABLE dept2 (
	id   NUMBER(7) NOT NULL,
	name VARCHAR2(25)
);

--2-2
INSERT INTO dept2 (id, name) SELECT
	                             department_id,
	                             department_name
                             FROM departments;

--2-3
CREATE TABLE emp2 (
	id         NUMBER(7)    NOT NULL,
	last_name  VARCHAR2(25) NOT NULL,
	first_name VARCHAR2(25) NOT NULL,
	dept_id    NUMBER(7)    NOT NULL
);

--2-4
ALTER TABLE emp2
	MODIFY (
	last_name VARCHAR2(50)
	);

--2-5
SELECT table_name
FROM sys.user_tables
WHERE table_name IN ('DEPT2', 'EMP2');

--2-6
CREATE TABLE employees2 AS (SELECT
	                            employee_id   AS id,
	                            first_name    AS first_name,
	                            last_name     AS last_name,
	                            salary        AS salary,
	                            department_id AS dept_id
                            FROM employees);

--2-7
DROP TABLE emp2;

--2-8
SELECT *
FROM recyclebin
WHERE original_name = 'EMP2';

--2-9
FLASHBACK TABLE emp2 TO BEFORE DROP;

--2-10
ALTER TABLE emp2
	DROP COLUMN last_name;

--2-11
ALTER TABLE employees2
	SET UNUSED (dept_id);

--2-12
ALTER TABLE employees2
	DROP UNUSED COLUMNS;

--2-13
ALTER TABLE emp2
	ADD CONSTRAINT my_emp_id_pk PRIMARY KEY (id);

--2-14
ALTER TABLE dept2
	ADD CONSTRAINT my_dept_id_pk PRIMARY KEY (id);

--2-15
ALTER TABLE emp2
	ADD CONSTRAINT my_emp_dept_id_fk FOREIGN KEY (dept_id) REFERENCES dept2 (id);

--2-16
SELECT
	constraint_name,
	constraint_type
FROM sys.user_constraints
WHERE table_name IN ('EMP2', 'DEPT2');

--2-17 ?
SELECT
	object_name,
	object_type
FROM sys.user_objects;

--2-18
ALTER TABLE emp2
	ADD (comission NUMBER(2, 2) )
	ADD CONSTRAINT emp2_comission_chk CHECK (comission > 0);

--2-19
DROP TABLE emp2 PURGE;
DROP TABLE dept2 PURGE;

--2-20
CREATE TABLE dept_named_index (
	deptno NUMBER(4) PRIMARY KEY USING INDEX (CREATE INDEX dept_pk_idx
		ON dept_named_index (deptno)),
	dname  VARCHAR2(30)
);

SELECT
	index_name,
	table_name
FROM sys.user_indexes
WHERE table_name = 'DEPT_NAMED_INDEX';

--3-1
CREATE TABLE sal_history
(
	employee_id NUMBER(6),
	hire_date   DATE,
	salary      NUMBER(8, 2)
);

--3-3
DROP TABLE mgr_history;
CREATE TABLE mgr_history
(
	employee_id NUMBER(6),
	manager_id  NUMBER(6),
	salary      NUMBER(8, 2)
);

--3-5
DROP TABLE special_sal;
CREATE TABLE special_sal
(
	employee_id NUMBER(6),
	salary      NUMBER(8, 2)
);

--3-7
INSERT ALL
WHEN (1 = 1) THEN
INTO sal_history VALUES (employee_id, hire_date, salary)
INTO mgr_history VALUES (employee_id, manager_id, salary)
WHEN (salary > 20000) THEN
INTO special_sal VALUES (employee_id, salary)
SELECT
	employee_id,
	hire_date,
	manager_id,
	salary
FROM employees
WHERE employee_id < 125;

--3-11
DROP TABLE sales_source_data;
CREATE TABLE sales_source_data
(
	employee_id NUMBER(6),
	week_id     NUMBER(2),
	sales_mon   NUMBER(8, 2),
	sales_tue   NUMBER(8, 2),
	sales_wed   NUMBER(8, 2),
	sales_thur  NUMBER(8, 2),
	sales_fri   NUMBER(8, 2)
);

--3-12
INSERT INTO sales_source_data VALUES
	(178, 6, 1750, 2200, 1500, 1500, 3000);

--3-15
DROP TABLE sales_info;
CREATE TABLE sales_info
(
	employee_id NUMBER(6),
	week        NUMBER(2),
	sales       NUMBER(8, 2)
);

--3-17
INSERT ALL
INTO sales_info VALUES (employee_id, week_id, sales_mon)
INTO sales_info VALUES (employee_id, week_id, sales_tue)
INTO sales_info VALUES (employee_id, week_id, sales_wed)
INTO sales_info VALUES (employee_id, week_id, sales_thur)
INTO sales_info VALUES (employee_id, week_id, sales_fri)
SELECT
	employee_id,
	week_id,
	sales_mon,
	sales_tue,
	sales_wed,
	sales_thur,
	sales_fri
FROM sales_source_data;

--3-18
SELECT *
FROM sales_info;

--3-19
DROP TABLE emp_data;
CREATE TABLE emp_data
(
	first_name VARCHAR2(25),
	last_name  VARCHAR2(25),
	email      VARCHAR2(45)
)
ORGANIZATION EXTERNAL (
TYPE ORACLE_LOADER
DEFAULT DIRECTORY stud
ACCESS PARAMETERS (
	RECORDS DELIMITED BY NEWLINE
		BADFILE stud:'geopela_emp_dat_bad'
	LOGFILE stud:'geopela_emp_dat_log'
	FIELDS TERMINATED BY '|'
	MISSING FIELD VALUES ARE NULL
	REJECT ROWS WITH ALL NULL FIELDS (
			first_name
			CHAR (25) LRTRIM,
			last_name
			CHAR (25) LRTRIM,
			email
			CHAR (45) LRTRIM
		))
LOCATION ('emp.dat')
) REJECT LIMIT 0;

SELECT *
FROM emp_data;

--3-20
CREATE TABLE emp_hist
	AS
		SELECT
			first_name,
			last_name,
			email
		FROM employees;

ALTER TABLE emp_hist
	MODIFY (
	email VARCHAR2(45)
	);

UPDATE emp_hist
SET email = nvl((SELECT email
                 FROM emp_data
                 WHERE emp_data.first_name = emp_hist.first_name
                       AND emp_data.last_name = emp_hist.last_name),
                emp_hist.email);

INSERT INTO emp_hist
	SELECT
		first_name,
		last_name,
		email
	FROM emp_data
	WHERE NOT exists(SELECT
		                 first_name,
		                 last_name
	                 FROM emp_hist
	                 WHERE emp_data.first_name = emp_hist.first_name
	                       AND emp_data.last_name = emp_hist.last_name);

--4-1
SELECT
	manager_id,
	job_id,
	sum(salary)
FROM employees
WHERE manager_id IS NOT NULL AND manager_id < 120
GROUP BY ROLLUP (manager_id, job_id);

--4-2
SELECT
	manager_id,
	job_id,
	sum(salary),
	grouping(manager_id),
	grouping(job_id)
FROM employees
WHERE manager_id IS NOT NULL AND manager_id < 120
GROUP BY ROLLUP (manager_id, job_id);

--ДОП:4 - 4

--4-3
SELECT
	manager_id,
	job_id,
	sum(salary) AS sum_salary
FROM employees
WHERE manager_id IS NOT NULL
GROUP BY CUBE (manager_id, job_id)
ORDER BY manager_id, job_id;

--4-4
SELECT
	manager_id,
	job_id,
	sum(salary) AS sum_salary,
	grouping(manager_id),
	grouping(job_id)
FROM employees
WHERE manager_id IS NOT NULL
GROUP BY CUBE (manager_id, job_id)
ORDER BY manager_id, job_id;

--4-5
SELECT
	department_id,
	manager_id,
	job_id,
	sum(salary) AS sum_salary
FROM employees
WHERE department_id IS NOT NULL
      AND manager_id IS NOT NULL
GROUP BY GROUPING SETS ((department_id, manager_id),
	(department_id, job_id),
	(manager_id, job_id))
ORDER BY department_id, manager_id, job_id;

--4-6
WITH raw_report AS
(SELECT
	 manager_id,
	 job_id,
	 count(employee_id)   AS emp_count,
	 sum(salary)          AS sum_salary,
	 grouping(job_id)     AS grouping_job_id,
	 grouping(manager_id) AS grouping_manager_id
 FROM employees
 WHERE manager_id IS NOT NULL
 GROUP BY ROLLUP (manager_id, job_id)
)
SELECT
	CASE (raw_report.grouping_manager_id)
	WHEN 0
		THEN (
			mgr_info.first_name ||
			' ' || mgr_info.last_name ||
			' ' || CASE WHEN (raw_report.grouping_job_id = 1)
				THEN 'итоги' END)
	WHEN 1
		THEN 'ОБЩИЙ ИТОГ' END       AS "Руководитель",

	CASE (raw_report.grouping_job_id)
	WHEN 0
		THEN emp_jobs.job_title
	WHEN 1
		THEN mgr_jobs.job_title END AS "Должность",

	raw_report.emp_count          AS "Количество подчиненных",

	sum_salary                    AS "Сумма"
FROM raw_report
	LEFT OUTER JOIN employees mgr_info ON (raw_report.manager_id = mgr_info.employee_id)
	LEFT OUTER JOIN jobs mgr_jobs ON (mgr_info.job_id = mgr_jobs.job_id)
	LEFT OUTER JOIN jobs emp_jobs ON (raw_report.job_id = emp_jobs.job_id)
ORDER BY raw_report.manager_id
