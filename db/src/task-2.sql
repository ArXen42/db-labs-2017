--2.7
WITH
	avg_salaries AS (SELECT department_id,
		                 round(avg(salary)) AS avg_salary
	                 FROM departments
		                      INNER JOIN employees USING (department_id)
	                 GROUP BY department_id, department_name),
	min_diffs    AS (SELECT department_id,
		                 avg_salary,
		                 min(abs(salary - avg_salary)) AS min_diff
	                 FROM avg_salaries
		                      INNER JOIN employees USING (department_id)
	                 GROUP BY department_id, avg_salary)
SELECT employee_id,
	last_name,
	job_id,
	salary,
	department_id,
	avg_salary
FROM min_diffs
	     INNER JOIN employees USING (department_id)
WHERE ABS(salary - avg_salary) = min_diff
ORDER BY department_id, salary, last_name
