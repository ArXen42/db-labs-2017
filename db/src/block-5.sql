--5-1
ALTER SESSION SET NLS_DATE_FORMAT = 'DD-MON-YYYY HH24:MI:SS';

--5-2
SELECT TZ_OFFSET('US/Pacific-New'),
       TZ_OFFSET('Singapore'),
       TZ_OFFSET('Egypt')
FROM dual;

ALTER SESSION SET TIME_ZONE = 'US/Pacific-New';

SELECT current_date,
       current_timestamp,
       localtimestamp
FROM dual;

ALTER SESSION SET TIME_ZONE = 'Singapore';

SELECT current_date,
       current_timestamp,
       localtimestamp
FROM dual;

--5-3
SELECT dbtimezone,
       sessiontimezone
FROM dual;

--5-4
SELECT extract(YEAR FROM hire_date) AS hire_year
FROM employees
WHERE department_id = 80;

--5-5
ALTER SESSION SET NLS_DATE_FORMAT = 'DD-MON-YYYY';

--5-6
WITH unfiltered_data(last_name, month, hire_date) AS
	     (
		     SELECT last_name,
		            extract(MONTH FROM hire_date),
		            hire_date
		     FROM employees
	     )
SELECT *
FROM unfiltered_data
WHERE month = 1;

--6-1
SELECT last_name,
       department_id,
       salary
FROM employees
WHERE (department_id, salary) IN (SELECT department_id,
                                         salary
                                  FROM employees
                                  WHERE commission_pct > 0);

--6-2
SELECT last_name,
       department_id,
       salary
FROM employees
WHERE (salary, nvl(commission_pct, 0)) IN (SELECT salary,
                                                  nvl(commission_pct, 0)
                                           FROM employees
	                                                INNER JOIN departments USING (department_id)
                                           WHERE location_id = 1700);

--6-3
WITH target_employee AS
	     (
		     SELECT employee_id,
		            salary,
		            commission_pct
		     FROM employees
		     WHERE last_name = 'Kochhar'
	     )
SELECT last_name,
       hire_date,
       salary
FROM employees
WHERE employee_id != (SELECT employee_id
                      FROM target_employee)
	AND (salary, nvl(commission_pct, 0)) = (SELECT salary,
	                                               nvl(commission_pct, 0)
	                                        FROM target_employee);

--6-4
WITH sa_mans AS
	     (
		     SELECT employee_id,
		            salary
		     FROM employees
		     WHERE job_id = 'SA_MAN'
	     )
SELECT last_name,
       job_id,
       salary
FROM employees
WHERE salary > ALL (SELECT salary
                    FROM sa_mans)
ORDER BY salary DESC;

--6-5
WITH target_cities_locations AS
	     (
		     SELECT location_id
		     FROM locations
		     WHERE city LIKE 'T%'
	     )
SELECT employee_id,
       last_name,
       department_id
FROM employees
	     INNER JOIN departments USING (department_id)
WHERE location_id IN (SELECT location_id
                      FROM target_cities_locations);

--6-6
WITH avg_salaries(department_id, avg_salary) AS
	     (
		     SELECT department_id,
		            avg(salary)
		     FROM employees
		     WHERE department_id IS NOT NULL
		     GROUP BY department_id
	     )
SELECT last_name     AS ename,
       salary        AS salary,
       department_id AS depno,
       avg_salary    AS dept_avg
FROM employees
	     INNER JOIN avg_salaries USING (department_id)
WHERE salary > avg_salary
ORDER BY avg_salary;

--6-7
SELECT last_name
FROM employees emps_outer
WHERE NOT exists(SELECT emps_inner.employee_id
                 FROM employees emps_inner
                 WHERE emps_inner.manager_id = emps_outer.employee_id);

--6-8
SELECT last_name
FROM employees
WHERE employee_id NOT IN (SELECT DISTINCT manager_id
                          FROM employees
                          WHERE manager_id IS NOT NULL);

--6-9
WITH avg_salaries(department_id, avg_salary) AS
	     (
		     SELECT department_id,
		            avg(salary)
		     FROM employees
		     WHERE department_id IS NOT NULL
		     GROUP BY department_id
	     )
SELECT last_name
FROM employees
	     INNER JOIN avg_salaries USING (department_id)
WHERE salary < avg_salary;

--6-10
SELECT last_name
FROM employees emps_outer
WHERE exists(SELECT employee_id
             FROM employees emps_inner
             WHERE emps_outer.department_id = emps_inner.department_id
		           AND emps_outer.salary < emps_inner.salary
		           AND emps_outer.hire_date < emps_inner.hire_date);

--6-11
SELECT employee_id,
       last_name,
       (SELECT department_name
        FROM departments
        WHERE employees.department_id = departments.department_id) AS department
FROM employees;

--6-12
WITH summary(total_salary) AS
	(
		SELECT sum(salary)
		FROM employees
	)
	 , depts_summary(department_id, dept_total) AS
	(
		SELECT department_id,
		       sum(salary)
		FROM employees
		WHERE department_id IS NOT NULL
		GROUP BY department_id
	)
SELECT department_name,
       dept_total
FROM depts_summary
	     INNER JOIN departments USING (department_id)
WHERE dept_total > (SELECT total_salary / 8
                    FROM summary);

--7-1
SELECT last_name,
       salary,
       department_id
FROM employees
START WITH last_name = 'Mourgos'
CONNECT BY PRIOR employee_id = manager_id;

--7-2
SELECT employee_id, last_name
FROM employees
WHERE employee_id != CONNECT_BY_ROOT employee_id
START WITH last_name = 'Lorentz'
CONNECT BY PRIOR manager_id = employee_id;

INSERT INTO employees (employee_id, first_name, last_name, email, phone_number, hire_date, job_id, salary, commission_pct, manager_id, department_id)
VALUES (1234, 'No one', 'Lorentz', 'email', 'phone', current_date, 'FI_ACCOUNT', 42, NULL, 108, 90);

DELETE
FROM employees
WHERE employee_id = 1234;

--7-3
SELECT lpad('_', 2 * level - 2, '_') || last_name AS name,
       manager_id                                 AS mgr,
       department_id                              AS deptno
FROM employees
START WITH last_name = 'Kochhar'
CONNECT BY PRIOR employee_id = manager_id;

--7-4
SELECT lpad(' ', level - 1) || employees.last_name AS last_name,
       employee_id,
       manager_id,
       job_id
FROM employees
START WITH employee_id = (SELECT employee_id
                          FROM employees
                          WHERE manager_id IS NULL)
CONNECT BY PRIOR employee_id = manager_id
       AND last_name != 'De Haan'
       AND job_id != 'ST_MAN';

--7-5
WITH given_date(given_dt) AS
	(
		SELECT to_date('14.12.2010', 'DD.MM.YYYY')
		FROM dual
	)
	 , dates(dt) AS
	(
		SELECT given_dt + level - 1
		FROM given_date
		CONNECT BY given_dt + level - 1 <= last_day(given_dt)
	)
SELECT to_char(dt, 'DD-MM-YYYY') AS dt,
       to_char(dt, 'Day')        AS day
FROM dates;

--7-6
WITH vals(val) AS
	(
		SELECT '(as)as)d((d)'
		FROM dual
		UNION ALL
		SELECT '(((as)d)'
		FROM dual
		UNION ALL
		SELECT '((as)dd))'
		FROM dual
		UNION ALL
		SELECT 'd((asdd))'
		FROM dual
		UNION ALL
		SELECT '()()'
		FROM dual
		UNION ALL
		SELECT '(()())'
		FROM dual
		UNION ALL
		SELECT ')()('
		FROM dual
		UNION ALL
		SELECT ' '
		FROM dual
		UNION ALL
		SELECT '()()()(((())))'
		FROM dual selec
	)
	 , numerated_vals(valnum, val) AS
	(
		SELECT rownum,
		       val
		FROM vals
	)
	 , cleaned_vals(valnum, val) AS
	(
		SELECT valnum,
		       translate(val, '()' || val, '()')
		FROM numerated_vals
		WHERE val LIKE '%(%'
	)
	 , reduced_vals(valnum, val) AS
	(
		SELECT valnum,
		       val
		FROM cleaned_vals

		UNION ALL

		SELECT valnum,
		       replace(val, '()')
		FROM reduced_vals
		WHERE val LIKE '%()%'
	)
	 , correct_valnums AS
	(
		SELECT valnum
		FROM reduced_vals
		WHERE val IS NULL
	)
SELECT val
FROM correct_valnums
	     INNER JOIN numerated_vals USING (valnum);

--7-7
WITH hierarchy_data(id, ref, val) AS
	     (
		     SELECT 'region_' || region_id,
		            NULL,
		            region_name
		     FROM regions

		     UNION ALL

		     SELECT 'country_' || country_id,
		            'region_' || region_id,
		            country_name
		     FROM countries

		     UNION ALL

		     SELECT 'city_' || location_id,
		            'country_' || country_id,
		            locations.city
		     FROM locations

		     UNION ALL
		     SELECT 'dep_' || department_id,
		            'city_' || location_id,
		            department_name
		     FROM departments
	     )
SELECT level                       AS "Уровень",
       lpad(' ', level - 1) || val AS "Единица"
FROM hierarchy_data
START WITH ref IS NULL
CONNECT BY PRIOR id = ref
ORDER SIBLINGS BY val;

--8-1
SELECT employee_id,
       first_name
FROM employees
WHERE regexp_like(first_name, '^N[a,e]*');

--8-2
SELECT regexp_replace(street_address, '\s', NULL) AS changed_address
FROM locations;

--8-3
SELECT street_address,
       REGEXP_REPLACE(street_address, '(\W)St($|\W)', '\1Street\2') AS fixed_address
FROM locations
WHERE REGEXP_LIKE(street_address, '(\W)St($|\W)');

--8-4
WITH paths(path) AS (
	SELECT 'C:\Windows\  ...          ..     System32\  .   ..            ...                       ...SomeFile'
	FROM dual
	UNION ALL
	SELECT '/etc/hosts'
	FROM dual)
SELECT path,
       substr(regexp_substr(path, '(\\|/)([^\\/])+$'), 2) AS filename
FROM paths;

-- 8-5
WITH strings(str) AS
	(
		SELECT 'er er  erer      erer er er ererer                            ererer er er erererer    erererer er er e e re re r r'
		FROM dual
	)
	 , normalized_strings(original_str, str) AS --Нормализация пробелов и начала/конца строки
	(
		SELECT str,
		       regexp_replace(trim(str), '(^|$|\W)+', '  ')
		FROM strings
	)
	 , duplicate_words(original_str, lvl, words) AS
	(
		SELECT DISTINCT original_str,
		                level,
		                regexp_substr(str, '(\s\w+\s)\1', 1, level, 'i')
		FROM normalized_strings
		CONNECT BY level <= regexp_count(str, '(\s\w+\s)\1', 1, 'i')
	)
SELECT original_str                    AS "Символьная строка",
       replace(trim(words), '  ', ' ') AS "Повторяющиеся слова"
FROM duplicate_words
ORDER BY original_str, lvl;

--8-6
WITH phone_numbers(num) AS
	     (
		     SELECT '(123) 343-22-33'
		     FROM dual
		     UNION ALL
		     SELECT '343-22-33'
		     FROM dual
		     UNION ALL
		     SELECT '123 343-22-33'
		     FROM dual
		     UNION ALL
		     SELECT '(12) 343-22-33'
		     FROM dual
	     )
SELECT num
FROM phone_numbers
WHERE regexp_like(num, '^(\(\d{3}\)\s)?' || '\d{3}-\d{2}-\d{2}$');

--8-7
WITH emails(email) AS
	     (
		     SELECT '1@ru'
		     FROM dual
	     )
SELECT email
FROM emails
WHERE REGEXP_LIKE(email,
                  '^((\d|\w)(\d|\w|\.|-)+)?(\d|\w)@' ||
                  '(((\d|\w)(\d|\w|-)*(\d|\w)\.)*((\d|\w)(\d|\w|-)*))?(\d|\w)$');

--8-8
WITH dat (txt) AS
	     (
		     SELECT 'Результатом вычисления выражения 2.5*3-6*5 будет число -22.5, а результатом вычисления выражения (3.0-5)-9*4 – число -28'
		     FROM dual
	     ),
     dat_normalized AS
	     (
		     SELECT trim(';' FROM regexp_replace(
				     regexp_replace(replace(txt, '+', '+ '), '[^0-9.+]', ';'),
				     ';+', ';')) AS txt
		     FROM dat
	     ),
     tokenized AS
	     (
		     SELECT regexp_substr(txt, '[^;]+', 1, level) token
		     FROM dat_normalized
		     CONNECT BY level <= length(regexp_replace(txt, '[^;]+')) + 1
	     ),
     all_numbers AS
	     (
		     SELECT DISTINCT to_number(replace(token, '+')) AS token
		     FROM tokenized
	     ),
     excluded AS
	     (
		     SELECT DISTINCT to_number(replace(token, '+'))
		     FROM tokenized
		     WHERE token LIKE '%+'
	     ),
     filtered_numbers AS
	     (
		     (SELECT *
		      FROM all_numbers)
		     MINUS
		     (SELECT * FROM excluded)
	     ),
     filtered_numbers_rownum AS
	     (
		     SELECT rownum AS num, token
		     FROM filtered_numbers
		     ORDER BY token
	     )
SELECT ltrim(sys_connect_by_path(token, ' ')) AS numbers
FROM filtered_numbers_rownum
WHERE connect_by_isleaf = 1
CONNECT BY PRIOR num = num - 1
START WITH num = 1;

-- 8-9
WITH dat (txt) AS
	     (
		     SELECT '222222222222222.222'
		     FROM dual
	     ),
     regexp (pattern) AS
	     (
		     SELECT '\D((\d+)|(\d{1,3}(((\s\d{3})+)|((,\d{3})+))))\.\d+'
		     FROM dual
	     ),
     tokenized AS
	     (
		     SELECT DISTINCT level                                                   AS lvl,
		                     txt,
		                     substr(regexp_substr(' ' || txt, pattern, 1, level), 2) AS num
		     FROM dat
			          CROSS JOIN regexp
		     CONNECT BY regexp_count(' ' || txt, pattern) >= level
		     ORDER BY 2, 1
	     )
SELECT CASE lvl WHEN 1 THEN txt ELSE ' ' END AS "Текст",
       num                                   AS "Результат"
FROM tokenized;


--8-10
WITH dat(txt) AS
	     (
		     SELECT '[[]][()]([])A   [[B[C][D]E[F[J]]H][K]L]M'
		     FROM dual
	     )
SELECT txt                                          AS original,
       regexp_replace(txt, '\[([^]\[]*)\]', '(\1)') AS result
FROM dat
