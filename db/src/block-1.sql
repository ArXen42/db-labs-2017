SELECT employee_id || ','
       || first_name || ','
       || employees.last_name || ','
       || email || ','
       || phone_number || ','
       || hire_date || ','
       || job_id || ','
       || salary || ','
       || commission_pct || ','
       || manager_id || ','
       || department_id
	AS the_output
FROM employees;


SELECT
	last_name,
	department_id
FROM employees
WHERE employee_id = 176;

SELECT
	last_name,
	salary
FROM employees
WHERE salary NOT BETWEEN 5000 AND 12000;

SELECT
	last_name,
	job_id,
	hire_date
FROM employees
WHERE hire_date BETWEEN DATE '2008-02-20' AND DATE '2008-05-01';

SELECT
	last_name,
	department_id
FROM employees
WHERE department_id IN (20, 50)
ORDER BY last_name;

SELECT
	last_name AS "Employee",
	salary    AS "Monthly Salary"
FROM employees
WHERE department_id IN (20, 50)
      AND salary BETWEEN 5000 AND 12000
ORDER BY last_name;

SELECT
	last_name,
	hire_date
FROM employees
WHERE EXTRACT(YEAR FROM hire_date) = 2004;

SELECT
	last_name,
	job_id
FROM employees
WHERE manager_id IS NULL;

-- 2-9
SELECT
	last_name,
	salary,
	commission_pct
FROM employees
WHERE commission_pct IS NOT NULL
ORDER BY salary DESC, commission_pct DESC;

-- 2-10
SELECT
	last_name,
	salary
FROM employees
WHERE salary > &tgt_salary;

-- 2-11
SELECT
	employee_id,
	last_name,
	salary,
	department_id
FROM employees
WHERE manager_id = &inp;

-- 2-12
SELECT DISTINCT last_name
FROM employees
WHERE last_name LIKE '__a%';

-- 2-13
SELECT DISTINCT last_name
FROM employees
WHERE last_name LIKE '%a%' AND last_name LIKE '%e%';

-- 2-14
SELECT
	last_name,
	job_id,
	salary
FROM employees
WHERE job_id IN ('SA_REP', 'ST_CLERK') AND salary NOT IN (2500, 3500, 7000);

-- 2-15
SELECT
	last_name AS "Employee",
	salary    AS "Monthly salary",
	commission_pct
FROM employees
WHERE commission_pct = 0.2;

-- 3-1-1
SELECT sysdate AS "Date"
FROM dual;

--3-1-2
SELECT
	employee_id,
	last_name,
	salary,
	round(salary * 0.1155) AS "New Salary"
FROM employees;

--3-1-4
SELECT
	employee_id,
	last_name,
	salary,
	round(salary * 0.1155)          AS "New Salary",
	round(salary * 0.1155) - salary AS "Increase"
FROM employees;

--3-1-5
SELECT DISTINCT
	last_name         AS "Last Name",
	length(last_name) AS "Length"
FROM employees
WHERE substr(last_name, 0, 1) IN ('J', 'A', 'M')
ORDER BY "Last Name";

--3-1-6
SELECT DISTINCT
	last_name         AS "Last Name",
	length(last_name) AS "Length"
FROM employees
WHERE last_name LIKE (&first_symbol) || '%'
ORDER BY "Last Name";

--3-1-7
SELECT round(MONTHS_BETWEEN(sysdate, hire_date)) AS months_worked
FROM employees
ORDER BY months_worked ASC;

--3-2-1
SELECT last_name
       || ' зарабатывает ' || trim(to_char(salary, '999,999,999,999.99L')) || ' в месяц'
       || ', но желает ' || trim(to_char(salary * 3, '999,999,999,999.99L'))
	AS "Dream Salaries"
FROM employees;

--3-2-2
SELECT
	last_name,
	replace(to_char(salary, '999999999999999'), ' ', '$') AS salary
FROM employees;

--3-2-3
SELECT
	last_name,
	hire_date,
	'Monday, ' || TO_CHAR(
			next_day(
					add_months(hire_date, 6),
					to_char(to_date('03/09/1982', 'dd/mm/yyyy'), 'DAY')),
			'Ddspth "of" MM, YYYY') AS review
FROM employees;

--3-2-4
SELECT
	last_name,
	hire_date,
	to_char(hire_date, 'DAY') AS day
FROM employees
ORDER BY to_number(to_char(hire_date, 'D'));

--3-2-5
SELECT
	last_name,
	commission_pct,
	NVL(to_char(commission_pct), 'No Comission') AS comm
FROM employees;

--3-2-6
SELECT rpad(last_name, 15) || lpad(' ', round(salary / 1000) + 1, '*')
	AS "EMPLOYEES AND THEIR SALARIES"
FROM employees
ORDER BY salary DESC;

--3-2-7
SELECT
	last_name,
	job_id,
	decode(job_id,
	       'AD_PRES', 'A',
	       'ST_MAN', 'B',
	       'IT_PROG', 'C',
	       'SA_REP', 'D',
	       'ST_CLERK', 'E',
	       '0') AS grade
FROM employees;

--3-2-8
SELECT
	last_name,
	job_id,
	CASE job_id
	WHEN 'AD_PRES'
		THEN 'A'
	WHEN 'ST_MAN'
		THEN 'B'
	WHEN 'IT_PROG'
		THEN 'C'
	WHEN 'SA_REP'
		THEN 'D'
	WHEN 'ST_CLERK'
		THEN 'E'
	ELSE '0' END
		AS grade
FROM employees;

-- 4-1
SELECT
	min(salary)        AS "Minimum",
	max(salary)        AS "Maximum",
	round(avg(salary)) AS "Average",
	sum(salary)        AS "Sum"
FROM employees;

-- 4-2
SELECT
	job_id,
	min(salary)        AS "Minimum",
	max(salary)        AS "Maximum",
	round(avg(salary)) AS "Average",
	sum(salary)        AS "Sum"
FROM employees
GROUP BY job_id;

-- 4-3
SELECT
	job_id,
	count(employee_id)
FROM employees
GROUP BY job_id;

-- 4-4
SELECT count(DISTINCT manager_id) AS "Number of Managers"
FROM employees;

-- 4-5
SELECT max(salary) - min(salary) AS "DIFFERENCE"
FROM employees;

-- 4-6
SELECT
	employees1.manager_id,
	min(employees1.salary) AS min_salary
FROM employees employees1 INNER JOIN employees employees2
		ON employees1.manager_id = employees2.employee_id
WHERE employees2.manager_id IS NOT NULL

GROUP BY employees1.manager_id
HAVING min(employees1.salary) >= 6000

ORDER BY min_salary DESC;

-- 4-7
SELECT
	count(employee_id) AS "Total",
	count(CASE extract(YEAR FROM hire_date)
	      WHEN 2005
		      THEN 1
	      END)         AS "2005",
	count(CASE extract(YEAR FROM hire_date)
	      WHEN 2006
		      THEN 1
	      END)         AS "2006",
	count(CASE extract(YEAR FROM hire_date)
	      WHEN 2007
		      THEN 1
	      END)         AS "2007",
	count(CASE extract(YEAR FROM hire_date)
	      WHEN 2008
		      THEN 1
	      END)         AS "2008"
FROM employees;

-- 4-8
SELECT
	job_id,
	nvl(to_char(sum(CASE department_id
	                WHEN 20
		                THEN salary
	                END)), ' ') AS "Dept20",
	nvl(to_char(sum(CASE department_id
	                WHEN 50
		                THEN salary
	                END)), ' ') AS "Dept50",
	nvl(to_char(sum(CASE department_id
	                WHEN 80
		                THEN salary
	                END)), ' ') AS "Dept80",
	nvl(to_char(sum(CASE department_id
	                WHEN 90
		                THEN salary
	                END)), ' ') AS "Dept90",
	nvl(sum(CASE department_id
	        WHEN 20
		        THEN salary
	        END), 0) +
	nvl(sum(CASE department_id
	        WHEN 50
		        THEN salary
	        END), 0) +
	nvl(sum(CASE department_id
	        WHEN 80
		        THEN salary
	        END), 0) +
	nvl(sum(CASE department_id
	        WHEN 90
		        THEN salary
	        END), 0)            AS "Total"
FROM employees
GROUP BY job_id;
