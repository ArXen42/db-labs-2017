-- 5.3
WITH hierarchy AS (
	SELECT emp1.employee_id AS employee_id,
	       emp2.employee_id AS manager_id,
	       emp1.last_name   AS emp_last_name,
	       emp2.last_name   AS mgr_last_name
	FROM employees emp1
		     LEFT OUTER JOIN employees emp2 ON (emp1.manager_id = emp2.employee_id)
)
SELECT ltrim(sys_connect_by_path(emp_last_name, '->'), '->') || ' (не имеет подчинённых)' AS man_list
FROM hierarchy
WHERE connect_by_isleaf = 1
CONNECT BY PRIOR employee_id = manager_id
START WITH manager_id IS NULL

